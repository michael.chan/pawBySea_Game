//
//  Extension+Dictionary.swift
//  bleGame
//
//  Created by SGI Mac Pro on 20/3/2018.
//  Copyright © 2018 SGI Mac Pro. All rights reserved.
//

import Foundation

extension Sequence where Iterator.Element == [String: Any] {
    
    var jsonString:String? {
        
        do {
            let stringData = try JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
            if let string = String(data: stringData, encoding: .utf8) {
                return string.replacingOccurrences(of: "\n", with: "")
            }
        }catch _ {
            
        }
        return nil
    }
}
