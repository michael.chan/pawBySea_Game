//
//  Extension+String.swift
//  bleGame
//
//  Created by SGI Mac Pro on 12/3/2018.
//  Copyright © 2018 SGI Mac Pro. All rights reserved.
//

import Foundation

extension String{
    var divided3Parts : [Double]{
        guard self.count == 12 else{
            return [0.0,0.0,0.0]
        }
        var temp = ""
        //self => ABCDEFGHIJKL
        for (i,char) in self.enumerated(){
            temp.append((i+1) % 2 == 0 ? "\(char):" : "\(char)")
        }
        //temp => AB:CD:EF:GH:IJ:KL
        //    print(temp)
        let result = temp.components(separatedBy:":")
        //return [CDAB,GHEF,KLIJ]
        let v1 = result[1]+result[0]
        let v2 = result[3]+result[2]
        let v3 = result[5]+result[4]
        let value1 = UInt16(v1,radix:16)!
        let doubleValue1 = value1 > 0x7fff ?  -Double(~value1 + 1) : Double(value1)
        let value2 = UInt16(v2,radix:16)!
        let doubleValue2 = value2 > 0x7fff ?  -Double(~value2 + 1) : Double(value2)
        let value3 = UInt16(v3,radix:16)!
        let doubleValue3 = value3 > 0x7fff ? -Double(~value3 + 1) : Double(value3)
        //        print("x",v1,doubleValue1)
        //         print("y",v2,doubleValue2)
        //         print("z",v3,doubleValue3)
        return [doubleValue1,doubleValue2,doubleValue3]
    }
}
