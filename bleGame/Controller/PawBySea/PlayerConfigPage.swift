//
//  PlayerConfigPage.swift
//  bleGame
//
//  Created by SGI Mac Pro on 22/3/2018.
//  Copyright © 2018 SGI Mac Pro. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher
import Hero

class PlayerConfigPage:UIViewController{
    
    @IBOutlet weak var p1id: UITextField!
    @IBOutlet weak var p2id: UITextField!
    @IBOutlet weak var p3id: UITextField!
    
    @IBOutlet weak var p1img: UIImageView!
    @IBOutlet weak var p2img: UIImageView!
    @IBOutlet weak var p3img: UIImageView!
    
    @IBOutlet weak var p1name: UILabel!
    @IBOutlet weak var p2name: UILabel!
    @IBOutlet weak var p3name: UILabel!
    
    var currentActive = 0
    
    var players: [Player] = GameplayInstance.players
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "configToGame"{
            let vc = segue.destination as! ViewController
            vc.players = GameplayInstance.players
            vc.onDoneBlock = { result in
                for i in 0..<3{
                  print("Return from game page")
                    self.players = GameplayInstance.players
                  self.refresh(player: i)
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hero.isEnabled = true
        
        
        self.p1img.hero.id = "player1chess"
        self.p1img.hero.modifiers = [.arc]
        self.p2img.hero.id = "player2chess"
        self.p2img.hero.modifiers = [.arc]
        self.p3img.hero.id = "player3chess"
        self.p3img.hero.modifiers = [.arc]
        
    }
    
    @IBAction func p1idChanged(_ sender: Any) {
        p1id.resignFirstResponder()
        ApiUsageClass.shared.delegate = self
        ApiUsageClass.shared.use(api: .getGameUser(id: Int(p1id.text ?? "0") ?? 0))
        currentActive = 0
    }
    @IBAction func p2idChanged(_ sender: Any) {
        p2id.resignFirstResponder()
        ApiUsageClass.shared.delegate = self
         ApiUsageClass.shared.use(api: .getGameUser(id: Int(p2id.text ?? "0") ?? 0))
        currentActive = 1
    }
    
    @IBAction func p3idChanged(_ sender: Any) {
        p3id.resignFirstResponder()
        ApiUsageClass.shared.delegate = self
         ApiUsageClass.shared.use(api: .getGameUser(id: Int(p3id.text ?? "0") ?? 0))
        currentActive = 2
    }
    
    @IBAction func pressGo(_ sender: Any) {
        var canGo = true
        for i in 0..<3{
            if GameplayInstance.players[i].id != 0{
                if !TagConnection.shared.tags[i].connected{
                    canGo = false
                    break
                }
            }
        }
        if canGo{
            self.performSegue(withIdentifier: "configToGame", sender: self)
        }
        else{
            let alert = UIAlertController(title: nil, message: "Some tags disconnected", preferredStyle: .alert)
            let cancel = UIAlertAction(title: "Cancel", style: .default, handler: { (action) in
                alert.dismiss(animated: true, completion: nil)
            })
            alert.addAction(cancel)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func pressBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func refresh(player: Int){
        
        switch player{
        case 0:
//            if let image1 = URL(string:players[0].photo){
//                p1img.kf.setImage(with: image1)
//            }else{
//                p1img.image = nil
//            }
             p1img.image = GameplayInstance.players[0].player
            
             p1name.text = GameplayInstance.players[0].name
        case 1:
            p2img.image = GameplayInstance.players[1].player

            p2name.text = GameplayInstance.players[1].name
        case 2:
            p3img.image = GameplayInstance.players[2].player
            p3name.text = GameplayInstance.players[2].name
        default:break
        }
        
        
        
        
    }
    
}
extension PlayerConfigPage: APIUsageDelegate{
    func onAPIRequestFeedback(request: ApiMode, feedback: ApiFeedback) {
       
        if case ApiMode.getGameUser(let id) = request{
            
            if case ApiFeedback.success(let response) = feedback{
                
                
                if let  content = response["content"] as? [String:Any]{
                    print("success:",content)
                    
                    GameplayInstance.players[currentActive].isActive = true
                    GameplayInstance.players[currentActive].id = id
                    GameplayInstance.players[currentActive].name = content["pet_name"] as! String
                    GameplayInstance.players[currentActive].photo =  content["pet_image"] as! String
                    if let url = content["pet_image"] as? String{
                        if let image = URL(string:url){
                            KingfisherManager.shared.retrieveImage(with: image, options: nil, progressBlock: nil, completionHandler: { (image, _, _, _) in
                                if let image = image{
                                    GameplayInstance.players[self.currentActive].updateImage(icon: image)
                                    self.refresh(player: self.currentActive)
                                }
                            })
                        }
                    }
//                        print("players \(currentActive)",players[currentActive].name)
                    refresh(player: currentActive)
                }
            }
            else{
                    
                
            }
            
        }
    }
    
}
