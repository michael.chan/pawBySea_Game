//
//  ViewController.swift
//  bleGame
//
//  Created by SGI Mac Pro on 28/2/2018.
//  Copyright © 2018 SGI Mac Pro. All rights reserved.
//

import UIKit
import CoreBluetooth
import BezierPathLength
import Alamofire
import Kingfisher
import Hero

class ViewController: UIViewController {
    @IBOutlet weak var TL1: UILabel!
    @IBOutlet weak var TL2: UILabel!
    @IBOutlet weak var TL3: UILabel!
    @IBOutlet weak var TL4: UILabel!
    @IBOutlet weak var TL5: UILabel!
    @IBOutlet weak var TL6: UILabel!
    @IBOutlet var waypoints: [UIView]!
    @IBOutlet var controlpoints: [UIView]!
    @IBOutlet weak var button: UIButton!
    @IBOutlet var chesses: [UIImageView]!
    @IBOutlet var championPage: UIView!
    @IBOutlet weak var championName: UILabel!
    
    @IBOutlet var TLNames: [UILabel]!
    @IBOutlet var TLPhoto: [UIImageView]!
    @IBOutlet var TLView: [UIView]!
    
    var showChampionPage = false{
        didSet{
            if showChampionPage{
                UIView.animate(withDuration: 0.5, animations: {
                    self.championPage?.alpha = 1
                })
            }else{
                UIView.animate(withDuration: 0.5, animations: {
                    self.championPage?.alpha = 0
                })
            }
        }
    }
    
    var timer = Timer()
    var isTimerRunning = false
    var currTime = 20
    var startTime = Date()
    
    var mPeripherals: [String:CBPeripheral] = [:]
    var prevIsPositive = [[false,false,false],[false,false,false],[false,false,false]]
    var prevValue:[[Double]] = [[0,0,0],[0,0,0],[0,0,0]]
    
    var players:[Player] = GameplayInstance.players
    
    var onDoneBlock : ((Bool) -> Void)?
    
    var score:[Int] = [0,0,0]{
        didSet{
            TL3.text = score[0].str
            TL4.text = score[1].str
            TL6.text = score[2].str
        }
    }
    
    var initialPos: CGPoint {
        return CGPoint(x:waypoints[0].layer.position.x,y:waypoints[0].layer.position.y - 128)
    }
    
    lazy var _path: UIBezierPath = {
       let _path = UIBezierPath()
        _path.move(to: waypoints[0].layer.position)
        for i in 1...(waypoints.count - 1) {
            _path.addQuadCurve(to: waypoints[i].layer.position, controlPoint: controlpoints[i-1].layer.position)
        }
        return _path
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.championPage?.alpha = 0
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissVC))
        self.championPage?.addGestureRecognizer(gesture)
        
        hero.isEnabled = true
        
        for i in 0..<3{
            self.chesses[i].hero.id = "player\(i+1)chess"
            self.chesses[i].hero.modifiers = [.arc]
        }
            
        TagConnection.shared.data = self
        
        TL1.text = players[0].name
        TL2.text = players[1].name
        TL5.text = players[2].name
        TLView = TLView.sorted { $0.tag < $1.tag }
        TLNames = TLNames.sorted { $0.tag < $1.tag }
        TLPhoto = TLPhoto.sorted { $0.tag < $1.tag }
        waypoints = waypoints.sorted { $0.tag < $1.tag }
        controlpoints = controlpoints.sorted{ $0.tag < $1.tag}
        
        for i in 0..<3{
            TLNames[i].text = GameplayInstance.players[i].name
            if let url = URL(string: GameplayInstance.players[i].photo){
                TLPhoto[i].kf.setImage(with: url)
                TLPhoto[i].image = TLPhoto[i].image?.circularImage(size: CGSize(width: 50, height: 50))
            }
        }
        
        start()

        //        for i in stride(from: 1, to: 100, by: 2){
        //            print("\(i)%",_path.cgPath.point(at: CGFloat(i) * 0.01))
        //        }
//        print(self._path.length)
    }
    
    @objc func dismissVC(){
        self.dismiss(animated: true, completion: {
            self.onDoneBlock?(true)
        })
    }
    
    @IBAction func pressStart(_ sender: Any) {
        let action = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        let restart = UIAlertAction(title: "Restart", style: .default) { (action) in
            if !self.isTimerRunning{
                self.start()
            }
        }
        let leave = UIAlertAction(title: "Leave", style: .destructive) { (action) in
            self.dismiss(animated: true, completion: nil)
        }
        action.addAction(restart)
        action.addAction(leave)
        self.present(action, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
//Gameplay
extension ViewController{
    @objc func timerTick(){
        self.currTime -= 1
        if self.currTime > 0{
            button.setTitle(self.currTime.str, for: .normal)
        }else{
            print("Time's up")
            stop()
//            for i in 0..<3{
//                print("Player \(i) position: \(chesses[i].layer.position)")
//                move(player: i, to: chesses[i].layer.position)
//            }
            
           
//            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func start(){
        if !isTimerRunning{
            reset()
            self.startTime = Date()
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: (#selector(self.timerTick)), userInfo: nil, repeats: true)
            timer.fire()
            self.isTimerRunning = true
        }
    }
    
    func stop(){
        for (i,_) in players.enumerated() where players[i].isActive{
            if players[i].time == 0.0{
                players[i].time = Double(GameplayPara.timer)
            }
        }
        button.setTitle("0", for: .normal)
        isTimerRunning = false
        timer.invalidate()
        report()
    }
    
    func reset(){
        currTime = GameplayPara.timer
        
        //Hide players which tag not connected
        for i in 0..<3{
            players[i].score = 0
            chesses[i].image = players[i].player
            
            
            chesses[i].isHidden = players[i].id == 0
        }
        
        //move players to initial Pos
//        for i in 0..<3{
//            move(player: i, to: initialPos)
//        }
        
        
        
    }
    
    func move(player i:Int, to point:CGPoint){
        print("Move player \(i) to point \(point.debugDescription)")
        chesses[i].layer.position = point
    }
    
    func report(){
        var msg: String = ""
        for i in 0..<3{
            msg += "\n\(players[i].name): \(Int(players[i].score))"
        }
    
//        let report = UIAlertController(title: "Scores", message: msg
//            , preferredStyle: .alert)
//        let confirm = UIAlertAction(title: "OK", style: .default) { (action) in
//
            var parameters: [String:Any] = [:]
            for (i,_) in self.players.enumerated(){
                let paraSingle: [String:Any] = [
                    "user_id\(i+1)":self.players[i].id,
                    "spend_time\(i+1)":String(self.players[i].time),
                    "process\(i+1)":String(self.players[i].process > 1 ? 1.0 : self.players[i].process),
                    "point\(i+1)": String(self.players[i].reachedPoint)
                ]

                parameters.merge(paraSingle.lazy.map { ($0.key, $0.value) }) { (_, new) in new }
            }
            ApiUsageClass.shared.delegate = self
            ApiUsageClass.shared.use(api: .postGameResult(results: parameters))
//
//
//        }
//        report.addAction(confirm)
//        self.present(report, animated: true, completion: nil)
    }
}

extension ViewController:TagUpdateValueDelegate{
    func didReturnValueFrom(peripheral: CBPeripheral, value: String) {
//        print(peripheral.identifier.uuidString, "return",value)
        let newSign: [Bool] = value.divided3Parts.map {$0 < 0}
        let newValue = value.divided3Parts
        
        for (i,tag) in TagConnection.shared.tags.enumerated() {
            if (tag.peripheral == peripheral){
//                if compare(value: newValue, with: prevValue[i]){
//                if newSign != prevIsPositive[i]{
                if compare(value: newValue, with: prevValue[i]) && newSign != prevIsPositive[i]{
                    if isTimerRunning{
                        players[i].score += GameplayPara.ptFor1chok
                       
                        let percentage = CGFloat(players[i].score) / CGFloat(GameplayPara.destinationScore)
                        if let point = _path.cgPath.point(at: percentage){
                            print(percentage)
                            move(player: i, to: CGPoint.init(x: point.x, y: point.y - 64))
                            for waypoint in self.waypoints{
                                if waypoint.layer.frame.contains(point) {
                                    players[i].reachedPoint = waypoint.tag
                                    if players[i].reachedPoint == 6{
                                        players[i].time = Date().timeIntervalSince(self.startTime)
                                        
                                    }
                                }
                                if (GameplayInstance.players.filter{$0.reachedPoint == 6}.count) == (GameplayInstance.players.filter{$0.id != 0}.count){
                                    self.stop()
                                }
                            }
                            self.prevIsPositive[i] = newSign
                            self.prevValue[i] = newValue
                        }
                    }
                }
            }
        }
    }
    
    func compare(value: [Double], with: [Double])->Bool{
        for i in 0..<3{
            if (value[i] - with[i]).magnitude > 300{
                return true
            }
        }
        return false
    }
}
extension ViewController:APIUsageDelegate{
    func onAPIRequestFeedback(request: ApiMode, feedback: ApiFeedback) {
        if case ApiMode.postGameResult(_) = request{
//        print(feedback)
            if case ApiFeedback.success(let mresult) = feedback{
                print(feedback)
                if let content = mresult["content"] as? [String:Any]{
                    print("content:",content)
                    if let winner = content["winner"] as? String{
                        print("winner:",winner)
//                        for i in 0..<3{
//                            if String(GameplayInstance.players[i].id) == winner{
//                                self.championName.text = GameplayInstance.players[i].name
//                            }
//                        }
                            self.championName.text = getChampionName()
                            self.showChampionPage = true
                            GameplayInstance.players = [Player.init(id: 0, name: "", photo: "", player: 0, icon: UIImage()) ,Player.init(id: 0, name: "", photo: "", player: 1, icon: UIImage()),Player.init(id: 0, name: "", photo: "", player: 2, icon: UIImage())]

    //
                        
                    }
                }
            }
//            else{
//                print("content type:",mresult["content"])
//            }
         }
    }
    
    func getChampionName()->String{
        var completePlayers: [Player] = []
        var notCompletePlayers: [Player] = []
        
        completePlayers = GameplayInstance.players.filter{$0.id != 0 && $0.time != 20.0}
        notCompletePlayers = GameplayInstance.players.filter{$0.id != 0 && $0.time == 20.0}
        
        if completePlayers.count > 0{
            if completePlayers.count == 1{
                return completePlayers[0].name
            }
            else{
                completePlayers = completePlayers.sorted(by:{$0.time < $1.time})
                return completePlayers[0].name
            }
        }else {
            if notCompletePlayers.count == 1{
                return notCompletePlayers[0].name
            }
            else{
                notCompletePlayers = notCompletePlayers.sorted(by:{$0.process > $1.process})
                return notCompletePlayers[0].name
            }
        }
    }
}
