//
//  GameMainPage.swift
//  bleGame
//
//  Created by SGI Mac Pro on 1/6/2018.
//  Copyright © 2018 SGI Mac Pro. All rights reserved.
//

import Foundation
import UIKit

class GameMainPage:UIViewController{
    @IBOutlet weak var leftMenu: UITableView!
    @IBOutlet weak var btnStartGame: UIButton!
    @IBOutlet var gamePreviews: [UIView]!

    var selectedGameIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        gamePreviews = gamePreviews.sorted(by:{$0.tag < $1.tag})
        gamePreviews.forEach { (view) in
            view.isHidden = true
        }
        leftMenu.dataSource = self
        leftMenu.delegate = self
        leftMenu.tableFooterView = UIView(frame: CGRect.zero)
        leftMenu.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        segue.destination.modalTransitionStyle = .crossDissolve
    }
    
    @IBAction func startSelectedGame(_ sender: Any) {
        
        guard self.selectedGameIndex != 0 else{
            return
        }
        
        switch selectedGameIndex {
        case 1:
            self.performSegue(withIdentifier: "segueGameMenuToPawBySea", sender: self)
        case 2:
            self.performSegue(withIdentifier: "segueGameMenuToPetbleCare", sender: self)
        default:
            break
        }
        
    }
    
}

extension GameMainPage:UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
}

extension GameMainPage:UITableViewDataSource{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : UITableViewCell!
        cell = tableView.dequeueReusableCell(withIdentifier: "leftMenuCell")
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: "leftMenuCell")
        }
        
        switch indexPath.row {
        case 0:
            cell.textLabel?.text = "帶着狗狗去旅行"
        case 1:
            cell.textLabel?.text = "寵物馬拉松"
        default: break
            //
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for gamePreview in gamePreviews{
            if indexPath.row == gamePreview.tag - 1{
                self.selectedGameIndex = gamePreview.tag
                gamePreview.isHidden = false
            }
            else{
                gamePreview.isHidden = true
            }
        }
    }
}
