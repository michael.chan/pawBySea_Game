//
//  PetbleCareRegPage.swift
//  bleGame
//
//  Created by SGI Mac Pro on 19/6/2018.
//  Copyright © 2018 SGI Mac Pro. All rights reserved.
//

import Foundation
import UIKit
import CoreBluetooth
import RealmSwift
import MessageUI

class PetbleCareRegPage: UIViewController{
    
    @IBOutlet weak var tfEmail: UITextField!
     @IBOutlet weak var checkbox: UIImageView!
     @IBOutlet weak var imgDog: UIImageView!
     @IBOutlet weak var imgCat: UIImageView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnConnectTagMenu: UIButton!
    @IBOutlet weak var tagMenu: UIView!
    @IBOutlet weak var tfTagAddr: UITextField!
    @IBOutlet weak var btnConnect: UIButton!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var btnTagMenu: UIButton!
    
    
    var ble = BLEManager.sharedInstance
    var tagIsConnected = false{
        didSet{
//            print("hey")
            DispatchQueue.main.async {
                self.btnTagMenu.setTitle(self.tagIsConnected ? "Disconnect":"Connect", for: .normal)
                self.btnTagMenu.setTitleColor(self.tagIsConnected ? UIColor.red:UIColor.blue, for: .normal)
            }
        }
    }
    
    var selectedPet = 0
    var checked = true{
        didSet{
            checkbox.image = UIImage(named: checked ? "checkbox_checked.png" : "checkbox.png")
        }
    }
    
    var tagMenuIsActive = false{
        didSet{
            tagMenu.alpha = tagMenuIsActive ? 1 : 0
        }
    }
    
    override func viewDidLoad(){
        super.viewDidLoad()
        checked = true
        setupGesture()
        setupStyleFor(tfEmail)
        TagConnection.shared.connection = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let tag1Addr = UserDefaults.standard.string(forKey: "tag1Addr"){
            self.tfTagAddr.text = tag1Addr
        }
        
    }
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "seguePetbleCareRegToGame"{
            let realm = try! Realm()
            let pbuser = PetbleCareUser()
            if tfEmail.text ?? "" != ""{
                
                pbuser.regTime = NSDate()
                pbuser.email = tfEmail.text ?? ""
                pbuser.wantAdv = checked
                
                
                try! realm.write {
                    realm.add(pbuser)
                }
            }
            
            let vc = segue.destination as! PetbleCareGamePage
            vc.type = selectedPet
            vc.emailAddr = tfEmail.text ?? ""
//            vc.mPeripheral = TagConnection.shared.tags[0].peripheral
            vc.modalTransitionStyle = .crossDissolve
            vc.onDoneBlock = { result,score,reachedPoint in
                self.tfEmail.text = nil
                print("result: \(score) reached \(reachedPoint)")
                
                try! realm.write {
                    pbuser.score = score
                    pbuser.reachedPoint = reachedPoint
                    realm.add(pbuser,update: .modified)
                }
            }
        }
    }
    
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true) {
            //
            
        }
    }
    @IBAction func connect(_ sender: Any) {
        appendTag()
        UserDefaults.standard.setValue(tfTagAddr.text!,forKey: "tag1Addr")
    }
    @IBAction func sharedb(_ sender: Any) {
        let picker = MFMailComposeViewController()
        picker.mailComposeDelegate = self
        picker.setToRecipients(["michael.chan@sgi-venture.com","jerry.ho@sgi-venture.com"])
//        picker.setToRecipients(["michael.chan@sgi-venture.com"])
        
        picker.setSubject("Petble Game Log - \(Date().dateString(format: "yyyy-MM-dd HH:mm:ss"))")
            
        picker.setMessageBody("", isHTML: true)
        if let csv = getRecordCsv(){
            picker.addAttachmentData(csv, mimeType: "text/csv", fileName: Date().dateString(format: "yyyy-MM-dd HH:mm:ss"))
        }
            
        present(picker, animated: true, completion: nil)
        
        
    }
    
    func getRecordCsv()->Data?{
        // Creating a string.
        let mailString = NSMutableString()
        mailString.append("Email, Start Time, Accept Advertisement,Score,Prize\n")
        
        let realm = try! Realm()
        let records = realm.objects(PetbleCareUser.self)
        for record in records{
            
            let prize = ["None","8折","8折","8折","8折","8折","8折","8折","7折"]
            
            mailString.append("\(record.email),\((record.regTime as Date).dateString(format: "yyyy-MM-dd HH:mm:ss")),\(record.wantAdv),\(record.score),\(prize[record.reachedPoint])\n")
        }

        // Converting it to NSData.
        let data = mailString.data(using: String.Encoding.utf8.rawValue, allowLossyConversion: false)

        // Unwrapping the optional.
        if let content = data {
            print("NSData: \(content)")
            return content
        }
        return nil
       
    }
    
    @IBAction func tagMenuToogle(_ sender: Any) {
        self.tagMenuIsActive = !self.tagMenuIsActive
    }
    
    func refresh(){
        btnTagMenu.setTitle(TagConnection.shared.tags.first(where: {$0.connected && $0.macAddress.lowercased() == tfTagAddr.text?.lowercased()}) != nil ? "Connect":"Disconnect", for: .normal)
            
        
    }
    
    func setupStyleFor(_ textField:UITextField){
        
        //Center placeholder
        let centeredParagraphStyle = NSMutableParagraphStyle()
        centeredParagraphStyle.alignment = .center
        let attributedPlaceholder = NSAttributedString(string: "請輸入你的電郵地址", attributes: [NSAttributedStringKey.paragraphStyle: centeredParagraphStyle])
        textField.attributedPlaceholder = attributedPlaceholder
        textField.textAlignment = .center
        
        
        
        //Round corner
        textField.layer.cornerRadius = textField.layer.frame.height / 2
//        textField.layer.cornerRadius = 4.0
        textField.layer.borderWidth = 2.0
        textField.layer.borderColor = UIColor.clear.cgColor
        textField.layer.masksToBounds = true
        textField.borderStyle = .roundedRect
    
    }
    
    func setupGesture(){
        let tgDog = UITapGestureRecognizer(target: self, action: #selector(self.redirectDog))
        let tgCat = UITapGestureRecognizer(target: self, action: #selector(self.redirectCat))
        let tgCB = UITapGestureRecognizer(target: self, action: #selector(self.check))
        imgDog.addGestureRecognizer(tgDog)
        imgCat.addGestureRecognizer(tgCat)
        checkbox.addGestureRecognizer(tgCB)
        
    }
    
    
    func validateReg()->Bool{
        let email = (self.tfEmail.text ?? "").trimmingCharacters(in: .whitespacesAndNewlines)
        //Check empty
        guard email != "" else{
            let controller = UIAlertController(title: "Warning!", message: "No Email Input", preferredStyle: .alert)
               let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
               controller.addAction(okAction)
               present(controller, animated: true, completion: nil)
            return false
        }
        //Check email format
        let emailRegEx = "(?:[a-zA-Z0-9!#$%\\&‘*+/=?\\^_`{|}~-]+(?:\\.[a-zA-Z0-9!#$%\\&'*+/=?\\^_`{|}" +
                "~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\" +
                "x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-" +
                "z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5" +
                "]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-" +
                "9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21" +
                "-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
        let emailTest = NSPredicate(format:"SELF MATCHES[c] %@", emailRegEx)
        guard emailTest.evaluate(with: email) else {
            let controller = UIAlertController(title: "Warning!", message: "Email Format Invalid", preferredStyle: .alert)
               let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
               controller.addAction(okAction)
               present(controller, animated: true, completion: nil)
            return false
        }
        
        //Check tag connected
//        guard TagConnection.shared.tags.first(where: {$0.connected}) != nil else {
//            let controller = UIAlertController(title: "Warning!", message: "No Tag connected", preferredStyle: .alert)
//               let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
//               controller.addAction(okAction)
//               present(controller, animated: true, completion: nil)
//            return false
//        }
        return true
    }
    
    @objc func redirectDog(){
        
        guard validateReg() else {
            return
        }
        tagMenuIsActive = false
        selectedPet = 0
        self.performSegue(withIdentifier: "seguePetbleCareRegToGame", sender: self)
    }
    @objc func redirectCat(){
        guard validateReg() else {
            return
        }
        tagMenuIsActive = false
        selectedPet = 1
        self.performSegue(withIdentifier: "seguePetbleCareRegToGame", sender: self)
    }
    
    @objc func check(){
        checked = !checked
    }
}

extension PetbleCareRegPage{
    func appendTag(){
        for tag in TagConnection.shared.tags{
            if let ph = tag.peripheral{
                ble.centralManager.cancelPeripheralConnection(ph)
            }
        }
        TagConnection.shared.tags.removeAll()
        sleep(1)
        TagConnection.shared.tags.append(Tags(macAddress: tfTagAddr.text ?? ""))
        TagConnection.shared.connect()
    }
}

extension PetbleCareRegPage: TagConnectionDelegate{
    func didScanFinish(nearbyTags: [String]) {
        print("nearbyTags: \(nearbyTags)")
    }
    
    func didConnected(peripheral: CBPeripheral) {
        print("ConnectTagPage didConnected",peripheral)
        for tag in TagConnection.shared.tags where tag.peripheral == peripheral{
            tag.connected = true
            print("start get value from",peripheral.identifier.uuidString)
            TagConnection.shared.getDataFrom(peripheral: peripheral)
        }
        tagIsConnected = true
    }
    
    func didAddedServiceDictFor(peripheral: CBPeripheral) {
        ble.writeData(to: peripheral, value: "68", serviceStringuuid: "FFF0", characteristicStringuuid: "FFF1")
        ble.writeData(to: peripheral, value: "39", serviceStringuuid: "FFF0", characteristicStringuuid: "FFF1")
        ble.readData(from: peripheral, serviceStringuuid: "FFA0", characteristicStringuuid: "FFA5")
        tagIsConnected = true
    }
    
    func didDisconnected(peripheral: CBPeripheral) {
        for tag in TagConnection.shared.tags where tag.peripheral == peripheral{
            tag.connected = false
        }
        tagIsConnected = false
    }
    
    
}

extension PetbleCareRegPage:MFMailComposeViewControllerDelegate{
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
         dismiss(animated: true, completion: nil)
    }
}
