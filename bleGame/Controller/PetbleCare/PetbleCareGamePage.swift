//
//  PetbleCareGamePage.swift
//  bleGame
//
//  Created by SGI Mac Pro on 4/6/2018.
//  Copyright © 2018 SGI Mac Pro. All rights reserved.
//

import UIKit
import CoreBluetooth
import BezierPathLength
import Alamofire
import Kingfisher
import Hero
import Lottie
//import CoreAnimator

class PetbleCareGamePage: UIViewController {

    @IBOutlet var waypoints: [UIView]!
    @IBOutlet var controlpoints: [UIView]!
    @IBOutlet var fireworks: [UIView]!
    
    @IBOutlet var lottieFirework: [UIView]!
    
    @IBOutlet weak var button: UIButton!

    @IBOutlet weak var pet: UIImageView!
    
    @IBOutlet var mark80: UIImageView!
    @IBOutlet var mark70: UIImageView!
    @IBOutlet var mark60: UIImageView!
    
    @IBOutlet weak var championPage: UIView!
    var player: Player!
    var type = 0
    var mPeripheral: CBPeripheral?
    @IBOutlet weak var discountNo: UILabel!
    
    @IBOutlet weak var activityPoint: UILabel!
    @IBOutlet weak var goButton: UIButton!
    
//    let fireworkqueue = DispatchQueue(label: "fireQueue" , attributes: .concurrent)
    let lottieView = LOTAnimationView(name: "firework")
    
    var countDownTimer = Timer()
    var label: UILabel!
    var countNo = 3
    var discountCode = ""{
        didSet{
            self.lblDiscountCode.text = self.discountCode
        }
    }
    @IBOutlet weak var lblDiscountCode: UILabel!
    
    var showChampionPage = false{
        didSet{
            if showChampionPage{
                
                UIView.animate(withDuration: 0.5, animations: {
                    self.championPage.alpha = 1
                })
            }else{
                UIView.animate(withDuration: 0.5, animations: {
                    self.championPage.alpha = 0
                })
            }
        }
    }
    
    var timer = Timer()
    var isTimerRunning = false
    var currTime = GameplayPara.timer+4
    var startTime = Date()
    var emailAddr = ""
    
    var canMove = false
    
    var mPeripherals: [String:CBPeripheral] = [:]
    
    var prevIsPositive = [[false,false,false],[false,false,false],[false,false,false]]
    var prevValue:[[Double]] = [[0,0,0],[0,0,0],[0,0,0]]
    
    var onDoneBlock : ((Bool,Int,Int) -> Void)?
    
    var score:[Int] = [0,0,0]{
        didSet{
            
        }
    }
    
    var initialPos: CGPoint {
        return CGPoint(x:waypoints[0].layer.position.x,y:waypoints[0].layer.position.y)
    }
    
    lazy var _path: UIBezierPath = {
        let _path = UIBezierPath()
        _path.move(to: waypoints[0].layer.position)
        for i in 1...(waypoints.count - 1){
            _path.addLine(to: waypoints[i].layer.position)
        }
        return _path
    }()
    
    @IBAction func onPress(_ sender: Any) {
        
        process()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.championPage.alpha = 0
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissVC))
        self.championPage.addGestureRecognizer(gesture)
        ApiUsageClass.shared.delegate = self
//        label = UILabel(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
//        label.center = self.view.center
//        label.font = UIFont.systemFont(ofSize: 100)
//        label.isHidden = true
//        self.view.addSubview(label)
        
//        countDownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: (#selector(self.countDown)), userInfo: nil, repeats: true)
        
        //"\\bleGame\\Store\\Gameplay\\PetbleCare\\" +
        
        
        let imagePath =  (type == 0 ? "PC_dog" : "PC_cat")
        pet.loadGif(name: imagePath)
        
        self.discountNo.text = "85"
        
//        pet.image = (type == 1 ? #imageLiteral(resourceName: "Cat Thumbnail.png") : #imageLiteral(resourceName: "Dog Thumbnail.png"))
        
        hero.isEnabled = true
        
        
        TagConnection.shared.data = self
        
        waypoints = waypoints.sorted { $0.tag < $1.tag }
        lottieFirework = lottieFirework.sorted { $0.tag < $1.tag }
//        fireworks = fireworks.sorted { $0.tag < $1.tag }
//        fireworks.forEach { (firework) in
//            firework.isHidden = true
//        }
//        controlpoints = controlpoints.sorted{ $0.tag < $1.tag}
        
       
        
        player = Player(id: 0, name: "", photo: "", player: 0, icon: UIImage())
        
//        start()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
//        countDown()
    }
    @IBAction func btnPressStart(_ sender: Any) {
        start()
    }
    
    @objc func dismissVC(){
        self.dismiss(animated: true, completion: {
            self.onDoneBlock?(true,self.player.score,self.player.reachedPoint)
        })
    }

    @IBAction func backToPrev(_ sender: Any) {
        
        dismissVC()
    }
    @IBAction func pressStart(_ sender: Any) {
        let action = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        let restart = UIAlertAction(title: "Restart", style: .default) { (action) in
            if !self.isTimerRunning{
                self.start()
            }
        }
        let leave = UIAlertAction(title: "Leave", style: .destructive) { (action) in
            self.dismiss(animated: true, completion: nil)
        }
        action.addAction(restart)
        action.addAction(leave)
        self.present(action, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func addFirework(to view:UIView){
        lottieView.removeFromSuperview()
        
        lottieView.layer.frame = view.bounds
        lottieView.loopAnimation = false
        lottieView.contentMode = .scaleAspectFit
        view.addSubview(lottieView)
        lottieView.play(completion: { (finished) in
            // Do Something
//            self.lottieView.removeFromSuperview()
        })
        
    }
    
}
//Gameplay
extension PetbleCareGamePage{
    @objc func timerTick(){
        
        if self.currTime > 0{
            
            if self.currTime > GameplayPara.timer{
                button.setTitle("", for: .normal)
                goButton.setTitle((self.currTime - GameplayPara.timer).str, for: .normal)
            }
            else if self.currTime == GameplayPara.timer{
//                button.setTitle("GO!", for: .normal)
                button.setTitle(GameplayPara.timer.str, for: .normal)
                goButton.isHidden = true
                canMove = true
            }
            else{
                button.setTitle(self.currTime.str, for: .normal)
            }
           
            
        }else{
            print("Time's up")
            stop()
            //            for i in 0..<3{
            //                print("Player \(i) position: \(chesses[i].layer.position)")
            //                move(player: i, to: chesses[i].layer.position)
            //            }
            
            
            //            self.dismiss(animated: true, completion: nil)
        }
        self.currTime -= 1
    }
    
    @objc func countDown(){
        
        if self.countNo > 0{
            self.label.isHidden = false
            UIView.animate(withDuration: 1) {
                self.label.alpha = 0.0
                self.label.text = "\(self.countNo)"
                self.label.alpha = 1.0
            }
            self.countNo -= 1
        }
        else{
            
            self.start()
            self.label.isHidden = true
        }
    }
    
    func start(){
        

//        sleep(1)
//        UIView.animate(withDuration: 1) {
//            self.label.alpha = 0.0
//            self.label.text = "2"
//            self.label.alpha = 1.0
//        }
//        sleep(1)
//        UIView.animate(withDuration: 1) {
//            self.label.alpha = 0.0
//            self.label.text = "1"
//            self.label.alpha = 1.0
//        }
//        UIView.animate(withDuration: 1) {
//            self.label.alpha = 0.0
//            self.label.text = "0"
//            self.label.alpha = 1.0
//        }
//        UIView.animate(withDuration: 1) {
//            self.label.alpha = 0.0
//
//        }
        
        
        if !isTimerRunning{
            reset()
            self.startTime = Date()
            UIView.animate(withDuration: 1) {
                let mPlayer = self.pet!
                mPlayer.layer.transform = CATransform3DMakeScale(3, 3, 1)
//                let point = self.waypoints[0].layer.position
//                self.move(to: CGPoint.init(x: point.x, y: point.y - 32))
            }
            
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: (#selector(self.timerTick)), userInfo: nil, repeats: true)
            timer.fire()
            self.isTimerRunning = true
        }
    }
    
    func stop(){
        
        // TODO: add api
        //ApiUsageClass.shared.use(api: ApiMode.postDiscount(email: "test@petble.com", discount: 20, token: "29f2ec9adfdebf72bbf64b568c647c6b"))
        var discount = 0
        if (player.reachedPoint >= 1) {discount = 15}
        if player.reachedPoint >= 7 {discount = 20}
        if player.reachedPoint >= 8 {discount = 25}
        
        api(email: emailAddr, discount: discount){
            code,dis in
            //self.discountCode = code
            DispatchQueue.main.async {
                self.lblDiscountCode.text = code
                self.discountNo.text = dis.str
            }
            
        }
        
        showChampionPage = true
        pet.isHidden = true
        canMove = false
        button.setTitle("0", for: .normal)
        isTimerRunning = false
        timer.invalidate()
        report()
    }
    
    func api(email:String,discount:Int,result: ((String,Int)->())?){
        // Create URL
        let url = URL(string: "https://www.espetsso.com/hk/marathon?email=\(email)&reduction_percent=\(discount)&token=29f2ec9adfdebf72bbf64b568c647c6b")
        guard let requestUrl = url else { fatalError() }
        // Create URL Request
        var request = URLRequest(url: requestUrl)
        // Specify HTTP Method to use
        request.httpMethod = "GET"
        // Send HTTP Request
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            // Check if Error took place
            if let error = error {
                print("Error took place \(error)")
                return
            }
            
            // Read HTTP Response Status code
            if let response = response as? HTTPURLResponse {
                print("Response HTTP Status code: \(response.statusCode)")
            }
            
            // Convert HTTP Response Data to a simple String
            if let data = data, let dataString = String(data: data, encoding: .utf8) {
                print("Response data string:\n \(dataString)")
                do {
                          if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary {
                               
                               // Print out entire dictionary
                            //print(convertedJsonIntoDict)
                               
                               // Get value by key
                            if let code = convertedJsonIntoDict["code"] as? String,let discount = convertedJsonIntoDict["reduction_percent"] as? Int{
                                
                                
                                var show = (100-discount)
                                if show % 10 == 0{
                                    show = show / 10
                                }
                                
                                   result?(code,show)
                                }
                           }
                } catch let error as NSError {
                           print(error.localizedDescription)
                 }
            }
            
        }
        task.resume()
    }
    
    func reset(){
        currTime = GameplayPara.timer + 3 //3 for countdown 3,2,1
        
        //Hide players which tag not connected
        
        
        //move players to initial Pos
    }
    
    func move(to point:CGPoint){
        print("Move player to point \(point.debugDescription)")
        DispatchQueue.main.async {
             self.pet.layer.position = point
        }
       
    }
    
    func process(){
        if isTimerRunning{
            if(canMove){
                player.score += GameplayPara.ptFor1chok
            }
            let percentage = CGFloat(player.score) / CGFloat(GameplayPara.destinationScore)
            if let point = _path.cgPath.point(at: percentage){
                print(percentage)
                self.activityPoint.text = String(Int(10000.0 * percentage))
                move(to: CGPoint.init(x: point.x, y: point.y - 32))
                for waypoint in self.waypoints{
                    if waypoint.layer.frame.contains(point) {
                        player.reachedPoint = waypoint.tag
                        switch waypoint.tag{
                        
                        case 1:
                            self.mark80.image = UIImage(named: "85%_red.png")
                            self.discountNo.text = "85"
//                                    firework(boom: 1)
                            addFirework(to: lottieFirework[0])
                        case 5,6:
//                                    self.pet.image = self.pet.image?.withHorizontallyFlippedOrientation()
                            self.pet.transform = CGAffineTransform(scaleX: -3, y: 3)
                        case 7:
                            self.mark70.image = UIImage(named: "80%_red.png")
                            self.discountNo.text = "8"
                            addFirework(to: lottieFirework[1])
                        case 8:
                            self.mark60.image = UIImage(named: "75%_red.png")
                            self.discountNo.text = "75"
                            addFirework(to: lottieFirework[2])
                            self.activityPoint.text = "10000"
                            self.stop()
                        default:break
                            
                        }
                    }
                    
                }
                
            }
        }
    }
    
    func report(){

    }
    
}

extension PetbleCareGamePage:TagUpdateValueDelegate{
    func didReturnValueFrom(peripheral: CBPeripheral, value: String) {
        //        print(peripheral.identifier.uuidString, "return",value)
        let newSign: [Bool] = value.divided3Parts.map {$0 < 0}
        let newValue = value.divided3Parts
        
        
        if (mPeripheral != nil && mPeripheral == peripheral){
            //                if compare(value: newValue, with: prevValue[i]){
            //                if newSign != prevIsPositive[i]{
            if compare(value: newValue, with: prevValue[0]) && newSign != prevIsPositive[0]{
                process()
                self.prevIsPositive[0] = newSign
                self.prevValue[0] = newValue
            }
        }
    
    }
    
    func compare(value: [Double], with: [Double])->Bool{
        for i in 0..<3{
            if (value[i] - with[i]).magnitude > 300{
                return true
            }
        }
        return false
    }
}
extension PetbleCareGamePage:APIUsageDelegate{
    func onAPIRequestFeedback(request: ApiMode, feedback: ApiFeedback) {
        if case ApiMode.postGameResult(_) = request{
                    print(feedback)
            if case ApiFeedback.success(let mresult) = feedback{
                print(feedback)
                if let content = mresult["content"] as? [String:Any]{
                    print("content:",content)
                    if let winner = content["winner"] as? String{
                        print("winner:",winner)

                        self.showChampionPage = true

                        
                    }
                }
            }
        }
        else if case ApiMode.postDiscount(_,_,_) = request{
            print("hi")
            if case ApiFeedback.success(let mresult) = feedback{
                print(feedback)
                if let content = mresult["code"] as? [String:Any]{
                    print("content:",content)
//                    if let winner = content["winner"] as? String{
//                        print("winner:",winner)
//
//                        //self.showChampionPage = true
//
//
//                    }
                }
            }
        }
    }
    
    func getChampionName()->String{
        var completePlayers: [Player] = []
        var notCompletePlayers: [Player] = []
        
        completePlayers = GameplayInstance.players.filter{$0.id != 0 && $0.time != 20.0}
        notCompletePlayers = GameplayInstance.players.filter{$0.id != 0 && $0.time == 20.0}
        
        if completePlayers.count > 0{
            if completePlayers.count == 1{
                return completePlayers[0].name
            }
            else{
                completePlayers = completePlayers.sorted(by:{$0.time < $1.time})
                return completePlayers[0].name
            }
        }else {
            if notCompletePlayers.count == 1{
                return notCompletePlayers[0].name
            }
            else{
                notCompletePlayers = notCompletePlayers.sorted(by:{$0.process > $1.process})
                return notCompletePlayers[0].name
            }
        }
    }
}
extension UIView {
    func fadeTransition(_ duration:CFTimeInterval) {
        let animation = CATransition()
        animation.timingFunction = CAMediaTimingFunction(name:
            kCAMediaTimingFunctionEaseInEaseOut)
        animation.type = kCATransitionFade
        animation.duration = duration
        layer.add(animation, forKey: kCATransitionFade)
    }
}

