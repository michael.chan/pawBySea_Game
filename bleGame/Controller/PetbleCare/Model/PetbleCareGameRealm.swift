//
//  PetbleCareGameRealm.swift
//  bleGame
//
//  Created by SGI Mac Pro on 24/6/2018.
//  Copyright © 2018 SGI Mac Pro. All rights reserved.
//

import Foundation
import RealmSwift

class PetbleCareUser:Object{
    
    @objc dynamic var regTimeStamp = Int(Date().timeIntervalSince1970)
    @objc dynamic var regTime = NSDate()
    @objc dynamic var email = ""
    @objc dynamic var wantAdv = false
    @objc dynamic var score = 0
    @objc dynamic var reachedPoint = 0
    
    override static func primaryKey() -> String? {
        return "regTimeStamp"
    }
}
