//
//  ConnectTagPage.swift
//  bleGame
//
//  Created by SGI Mac Pro on 4/3/2018.
//  Copyright © 2018 SGI Mac Pro. All rights reserved.
//

import UIKit
import CoreBluetooth

class ConnectTagPage: UIViewController {
  

    @IBOutlet weak var player1macAddr: UITextField!
    @IBOutlet weak var player2macAddr: UITextField!
    @IBOutlet weak var player3macAddr: UITextField!
    
    @IBOutlet weak var player1connection: UILabel!
    @IBOutlet weak var player2connection: UILabel!
    @IBOutlet weak var player3connection: UILabel!
    
    var ble = BLEManager.sharedInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let tag1Addr = UserDefaults.standard.string(forKey: "tag1Addr"){
            self.player1macAddr.text = tag1Addr
        }
        if let tag2Addr = UserDefaults.standard.string(forKey: "tag2Addr"){
            self.player2macAddr.text = tag2Addr
        }
        if let tag3Addr = UserDefaults.standard.string(forKey: "tag3Addr"){
            self.player3macAddr.text = tag3Addr
        }
        player1macAddr.delegate = self
        player2macAddr.delegate = self
        player3macAddr.delegate = self
        
        appendTag()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        TagConnection.shared.connection = self
        refresh()
    }
    
    func appendTag(){
        for tag in TagConnection.shared.tags{
            if let ph = tag.peripheral{
                ble.centralManager.cancelPeripheralConnection(ph)
            }
        }
        TagConnection.shared.tags.removeAll()
        sleep(1)
        TagConnection.shared.tags.append(Tags(macAddress: player1macAddr.text ?? ""))
        TagConnection.shared.tags.append(Tags(macAddress: player2macAddr.text ?? ""))
        TagConnection.shared.tags.append(Tags(macAddress: player3macAddr.text ?? ""))
    }
    
    @IBAction func btnPressConnectAll(_ sender: Any) {
        appendTag()
        TagConnection.shared.connect()
        self.player1macAddr.resignFirstResponder()
        self.player2macAddr.resignFirstResponder()
        self.player3macAddr.resignFirstResponder()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @objc func refresh(){
//        appendTag()
        for tag in TagConnection.shared.tags{
            if tag.macAddress.lowercased() == player1macAddr.text?.lowercased(){
                player1connection.text = tag.connected ? "Connected" : "Disconnected"
            }else if tag.macAddress.lowercased() == player2macAddr.text?.lowercased(){
                player2connection.text = tag.connected ? "Connected" : "Disconnected"
            }else if tag.macAddress.lowercased() == player3macAddr.text?.lowercased(){
                player3connection.text = tag.connected ? "Connected" : "Disconnected"
            }
        }
    }
    
}

extension ConnectTagPage: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.isFirstResponder{
            textField.resignFirstResponder()
            return true
        }
        return false
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField.tag{
        case 1:
            UserDefaults.standard.set(textField.text!, forKey: "tag1Addr")
        case 2:
            UserDefaults.standard.set(textField.text!, forKey: "tag2Addr")
        case 3:
            UserDefaults.standard.set(textField.text!, forKey: "tag3Addr")
        default:break
        }
    }
}

extension ConnectTagPage: TagConnectionDelegate{
    func didScanFinish(nearbyTags: [String]) {
        print("nearbyTags: \(nearbyTags)")
    }
    
    func didConnected(peripheral: CBPeripheral) {
        print("ConnectTagPage didConnected",peripheral)
        for tag in TagConnection.shared.tags where tag.peripheral == peripheral{
            tag.connected = true
            print("start get value from",peripheral.identifier.uuidString)
            TagConnection.shared.getDataFrom(peripheral: peripheral)
        }
        refresh()
    }
    
    func didAddedServiceDictFor(peripheral: CBPeripheral) {
        ble.writeData(to: peripheral, value: "68", serviceStringuuid: "FFF0", characteristicStringuuid: "FFF1")
        ble.writeData(to: peripheral, value: "39", serviceStringuuid: "FFF0", characteristicStringuuid: "FFF1")
        ble.readData(from: peripheral, serviceStringuuid: "FFA0", characteristicStringuuid: "FFA5")
       
    }
    
    func didDisconnected(peripheral: CBPeripheral) {
        for tag in TagConnection.shared.tags where tag.peripheral == peripheral{
            tag.connected = false
        }
        refresh()
    }
    
    
}
