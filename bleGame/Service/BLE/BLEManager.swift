//
//  BLEManager.swift
//  bleGame
//
//  Created by SGI Mac Pro on 28/2/2018.
//  Copyright © 2018 SGI Mac Pro. All rights reserved.
//

import Foundation
import CoreBluetooth

public protocol BLEManagerDelegate {
    //TODO make support 9.0
    func didDiscover(peripheral: CBPeripheral, advertisementData: [String : Any], RSSI: NSNumber)
    func didDiscoverPeripheralFinish()
    func didConnect(peripheral:CBPeripheral)
    func didFailToConnectPeripheral()
    func didDisconnect(peripheral:CBPeripheral)
    func didDiscoverCharacteristicsFor(peripheral:CBPeripheral)
    func didUpdateValueFor(peripheral:CBPeripheral, characteristic: CBCharacteristic, result:[UInt8])
    func didBLEStateChanged(central:CBCentralManager)
    
}



public class BLEManager: NSObject, CBCentralManagerDelegate, CBPeripheralDelegate,CBPeripheralManagerDelegate{
    public func peripheralManagerDidUpdateState(_ peripheral: CBPeripheralManager) {
        if peripheral.state == .poweredOn || TARGET_OS_SIMULATOR != 0{
            checkStateClosure!(true)
        } else {
            checkStateClosure!(false)
        }
    }
    var perMan:CBPeripheralManager!
    
    static let sharedInstance = BLEManager() //Swift Singleton Pettern
    
    var checkStateClosure : ((Bool)->())?
    public func isBleOn(_ feedback:@escaping (Bool)->()){
        self.checkStateClosure = feedback
        perMan = CBPeripheralManager(delegate: self, queue: nil, options: nil)
    }
    
    public var delegate: BLEManagerDelegate? //Delegate
    
    public var centralManager: CBCentralManager!   //CoreBluetooth
    
    private var mPeripheral:CBPeripheral! //Current connect device
    
    private var serviceDict: [String: [String:CBCharacteristic]] = [:] //store Characteristic
    
    private var serviceCount : Int = 0 //for check serviceDict is complete added
    
    var retryCount: Int = 0
    
    override public init() {
        super.init()
        if (self.centralManager == nil){
//            AppLog.log(tags:"BLE","BLE_Manager", content: "No Central Manager Object, create now")
            self.centralManager = CBCentralManager(delegate: self, queue: nil)
        }
    }
    
    var readyToConnectSignal : (()->())?
    var task :DispatchWorkItem!
    public func scanDevice(timeout: TimeInterval = 3.0){
        
//        AppLog.log(tags:"BLE","BLE_Manager", content: "Start scanning nearby device...")
//        AppLog.log(tags:"BLE","BLE_Manager", content: "Nearby Devices")
        task = DispatchWorkItem{
            self.stopScan()
//            AppLog.log(tags:"BLE","BLE_Manager", content: "Timeout, Stop Scan.")
            self.task.cancel()
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + timeout, execute: task)
        if case .poweredOn = self.centralManager.state{
            self.centralManager.scanForPeripherals(withServices: nil, options: nil)
        }
            
        else{
            revokeCenterManager {
                self.scanDevice(timeout:10)
            }
        }
        
    }
    public func revokeCenterManager(finish:@escaping ()->()){
        self.readyToConnectSignal = finish
//        AppLog.log(tags:"BLE","BLE_Manager", content: "Try Revoke CBCentralManager")
        self.centralManager = CBCentralManager(delegate: self, queue: nil)
        sleep(2) // wait 2 second for centralManager to load
    }
    
    public func stopScan(){
//        AppLog.log(tags:"BLE","BLE_Manager", content: "Stop scanning nearby device...")
        self.centralManager.stopScan()
        self.delegate?.didDiscoverPeripheralFinish()
    }
    
    public func connectPeripheral(peripheral: CBPeripheral){
        self.mPeripheral = peripheral
        if case .poweredOn = self.centralManager.state{
            //            self.centralManager.scanForPeripherals(withServices: nil, options: nil)
//            AppLog.log(tags:"BLE","BLE_Manager", content: "connecting \(self.mPeripheral)...")
            
            self.centralManager.connect(peripheral, options: nil)
//            if(self.centralManager.isScanning){
//                self.centralManager.stopScan()
//            }
        }
            
        else{
            guard retryCount < 5 else{
                self.stopScan()
                return
            }
            revokeCenterManager {
                sleep(2)
//                AppLog.log(tags:"BLE","BLE_Manager", content: "Try Revoke CBCentralManager")
                self.retryCount += 1
                self.connectPeripheral(peripheral: peripheral)
            }
        }
        
    }
    
    public func releaseObject() {
        self.serviceCount = 0
        if centralManager.isScanning{
            self.centralManager.stopScan()
        }
        if (self.mPeripheral != nil){
            self.centralManager.cancelPeripheralConnection(mPeripheral)
            self.mPeripheral = nil
        }
        self.serviceDict = [:]
        self.delegate = nil
    }
    
    // MARK: CBCentralManagerDelegate
    public func centralManagerDidUpdateState(_ central: CBCentralManager){
//        AppLog.log(tags:"BLE","PetbleControl","session_start", content: "")
//        AppLog.log(tags:"BLE","BLE_Manager", content: "Central manager state: \(String(describing: CBManagerState.init(rawValue: central.state.rawValue)))")
        switch central.state {
        case .unknown:
            break
        case .resetting:
            //            print("Central manager state: Resetting")
            break
        case .unsupported:
            //            print("Central manager state: Unsupported")
            
            break
        case .unauthorized:
            //            print("Central manager state: Unauthorized")
            
            break
        case .poweredOff:
            //            print("Central manager state: Powered off")
            
            break
        case .poweredOn:
            //            print("Central manager state: Powered on")
            self.readyToConnectSignal?()
            break
        }
        //        delegate?.didBLEStateChanged(central: central)
    }
    
    
    public func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        
        
        if peripheral.name != nil{
            //should check device name isNull
            delegate?.didDiscover(peripheral: peripheral, advertisementData: advertisementData, RSSI: RSSI)
        }
    }
    
    public func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        task.cancel()
        print("didConnected",peripheral.identifier.uuidString)
        peripheral.delegate = self
        peripheral.discoverServices(nil)
        delegate?.didConnect(peripheral: peripheral)
    }
    
    public func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?){
//        AppLog.log(tags:"BLE","BLE_Manager", content: "Fail To Connect \(peripheral.name!): \(peripheral.identifier.uuidString)")
        delegate?.didFailToConnectPeripheral()
    }
    
    public func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
//        AppLog.log(tags:"BLE","BLE_Manager","session_end", content: "Disconnected \(peripheral.name!): \(peripheral.identifier.uuidString)")
        delegate?.didDisconnect(peripheral: peripheral)
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        //        AppLog.log(tags:"BLE","BLE_Manager", content: "didDiscoverServices \(peripheral.name!): \(peripheral.identifier.uuidString)")
        
        if error == nil {
            serviceCount = peripheral.services!.count
            for service : CBService in peripheral.services! { //just loop discover characteristics
                peripheral.discoverCharacteristics(nil, for: service)
            }
        }
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        //         AppLog.log(tags:"BLE","BLE_Manager", content: "Discovered service uuids: \(service.uuid.uuidString)")
        
        var tmpDict : [String:CBCharacteristic] = [:]
        for characteristics : CBCharacteristic in service.characteristics!{ //add to dictionary
            //            AppLog.log(tags:"BLE","BLE_Manager", content: "Discovered characteristics uuids: \(characteristics.uuid.uuidString)")
            tmpDict[characteristics.uuid.uuidString] = characteristics
        }
        
        serviceDict[service.uuid.uuidString] = tmpDict
        if serviceDict.count == serviceCount { //check the dictionary is added all characteristics
            delegate?.didDiscoverCharacteristicsFor(peripheral:peripheral)
        }
    }
    
    
    
    //#warning setup return method
    public func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
//        AppLog.log(tags:"BLE","BLE_Manager", content: "didUpdateValueFor")
        if let u8s : [UInt8] =  characteristic.value?.u8s{
            //            AppLog.log(tags:"BLE","BLE_Manager", content: "\(characteristic.uuid.uuidString) update value: \(u8s)")
//            AppLog.log(tags:"BLE","BLE_Manager", content: "Read Characteristic: \(characteristic.uuid.uuidString)")
            //            AppLog.log(tags:"BLE","BLE_Manager", content: "-> Response: \(u8s)")
            delegate?.didUpdateValueFor(peripheral: peripheral, characteristic: characteristic, result: u8s)
        }else{
            //            AppLog.log(tags:"BLE","BLE_Manager", content: "\(characteristic.uuid.uuidString) no value update")
//            AppLog.log(tags:"BLE","BLE_Manager", content: "Read .o Response")
            delegate?.didUpdateValueFor(peripheral: peripheral, characteristic: characteristic, result: [UInt8]())
        }
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
        if (error != nil) {
//            AppLog.log(tags:"BLE","BLE_Manager", content: "didWriteValueForCharacteristic \(String(describing: error)) characteristic uuid:\(characteristic.uuid.uuidString)")
        }
    }
    
    /**
     Write data to peripheral
     
     - parameter value:                    Hex String
     - parameter serviceStringuuid:        uuid
     - parameter characteristicStringuuid: uuid
     */
    public func writeData(to peripheral: CBPeripheral,value : String, serviceStringuuid : String, characteristicStringuuid : String , withResponse: Bool = true){
      
//        AppLog.log(tags:"BLE","BLE_Manager", content: "Write to Characteristic: \(characteristicStringuuid)")
//        AppLog.log(tags:"BLE","BLE_Manager", content: "-> Command: \(value)")
        if let characteristic : CBCharacteristic = serviceDict[serviceStringuuid]?[characteristicStringuuid] {
            print("Write: \(value) to Service \(serviceStringuuid) Characteristic \(characteristicStringuuid)")
            
            withResponse ?
                peripheral.writeValue(dataWith(hex: value), for: characteristic, type: CBCharacteristicWriteType.withResponse) :
                peripheral.writeValue(dataWith(hex: value), for: characteristic, type: CBCharacteristicWriteType.withoutResponse)
        }else{
//            AppLog.log(tags:"BLE","BLE_Manager", content: "Not found characteristic uuid:\(characteristicStringuuid)")
        }
    }
    
    public func readData(from peripheral:CBPeripheral ,serviceStringuuid : String, characteristicStringuuid : String){
        if let characteristic : CBCharacteristic = serviceDict[serviceStringuuid]?[characteristicStringuuid] {
            print("Read from Service \(serviceStringuuid) Characteristic \(characteristicStringuuid)")
//            AppLog.log(tags:"BLE","BLE_Manager", content: "Read value for characteristic uuid:\(characteristicStringuuid)...")
            peripheral.readValue(for: characteristic)
            if characteristic.properties.intersection(CBCharacteristicProperties.notify) != []{
                peripheral.setNotifyValue(true, for: characteristic)
            }
            
        }else{
//            AppLog.log(tags:"BLE","BLE_Manager", content: "Not found characteristic uuid:\(characteristicStringuuid)")
        }
    }
    
    //Change Hex string to NSData
    @available(*,deprecated,message: "dataWithHexString(hex:) is changed to dataWith(hex:)")
    func dataWithHexString(hex:String)->Data{return Data()}
    
    func dataWith(hex: String) -> Data {
        var hex = hex
        var data = Data()
        while(hex.count > 0) {
            // swift 3.0
            //            let c: String = hex.substring(to: hex.index(hex.startIndex, offsetBy: 2))
            //            hex = hex.substring(from: hex.index(hex.startIndex, offsetBy: 2))
            
            //swift 4.0
            let index = hex.index(hex.startIndex, offsetBy: 2)
            let c : String = String(hex[..<index])
            hex = String(hex[index...])
            
            var ch: UInt32 = 0
            Scanner(string: c).scanHexInt32(&ch)
            var char = UInt8(ch)
            data.append(&char, count: 1)
        }
        return data
    }
    
}
extension Data {
    var u8s:[UInt8] { // Array of UInt8, Swift byte array basically
        var buffer:[UInt8] = [UInt8](repeating: 0, count: self.count)
        self.copyBytes(to: &buffer, count: self.count)
        return buffer
    }
}
extension Dictionary {
    mutating func update(other:Dictionary) {
        for (key,value) in other {
            self.updateValue(value, forKey:key)
        }
    }
}
