//
//  TagConnection.swift
//  bleGame
//
//  Created by SGI Mac Pro on 4/3/2018.
//  Copyright © 2018 SGI Mac Pro. All rights reserved.
//

import Foundation
import CoreBluetooth

protocol TagConnectionDelegate: class{
    func didScanFinish(nearbyTags:[String])
    func didConnected(peripheral:CBPeripheral)
    func didAddedServiceDictFor(peripheral:CBPeripheral)
    func didDisconnected(peripheral:CBPeripheral)
}

protocol TagUpdateValueDelegate:class {
    func didReturnValueFrom(peripheral:CBPeripheral,value:String)
}

class TagConnection{
    static let shared = TagConnection()
    var tags: [Tags] = []
    var scannedTags: [String]=[]
    
    var ble = BLEManager.sharedInstance
    weak var connection:TagConnectionDelegate?
    weak var data:TagUpdateValueDelegate?
    
     public var centralManager: CBCentralManager!   //CoreBluetooth
    
    init(){

        ble.delegate = self
        ble.scanDevice()
    }
    
    func connect(){

//        if BLEManager.sharedInstance.centralManager == nil{
        print("connecting")
        for tag in tags{
            print(tag.macAddress)
        }
        ble.delegate = self
//        BLEManager.sharedInstance.revokeCenterManager {
            ble.scanDevice()
//        }
//        }
//        else{
//            BLEManager.sharedInstance.scanDevice()
//        }
    }
    func getDataFrom(peripheral:CBPeripheral){
        ble.writeData(to: peripheral, value: "68", serviceStringuuid: "FFF0", characteristicStringuuid: "FFF1")
        ble.writeData(to: peripheral, value: "39", serviceStringuuid: "FFF0", characteristicStringuuid: "FFF1")
        ble.readData(from: peripheral, serviceStringuuid: "FFA0", characteristicStringuuid: "FFA5")
    }
}

extension TagConnection: BLEManagerDelegate{
    func didDiscoverCharacteristicsFor(peripheral: CBPeripheral) {
        print("Added serviceDict for",peripheral.identifier.uuidString)
        connection?.didAddedServiceDictFor(peripheral: peripheral)
    }
    
    func didDiscover(peripheral: CBPeripheral, advertisementData: [String : Any], RSSI: NSNumber) {
        var tmpToken : String = ""
        
        if let plName = advertisementData[CBAdvertisementDataLocalNameKey] as? String{
            if (plName.contains("Chongmi")) || (plName.contains("PETBLE STag")){
                //                print("\(advertisementData["kCBAdvDataServiceUUIDs"])")
                
                if let mArray : [CBUUID] = advertisementData["kCBAdvDataServiceUUIDs"] as? [CBUUID] {
                    for item in (mArray.count == 3 ? mArray.reversed() : mArray){
                        tmpToken += item.uuidString
                    }
                    TagConnection.shared.scannedTags.append(tmpToken)
                    for tag in TagConnection.shared.tags where tag.macAddress.lowercased() == tmpToken.lowercased(){
                        print("##id:",peripheral.identifier.uuidString, "macAddr:",tag.macAddress, "discovered")
                        print("pending to connect tag with macAddress",tag.macAddress.lowercased())
                        tag.peripheral = peripheral
                    }
                }
            }
        }
        
    }
    
    func didDisconnect(peripheral: CBPeripheral) {
        print("disconnected \(peripheral.identifier.uuidString)")
        connection?.didDisconnected(peripheral: peripheral)
    }
    
    func didUpdateValueFor(peripheral: CBPeripheral, characteristic: CBCharacteristic, result: [UInt8]) {
//        print("\(peripheral) value updated:",result)
        let raw = result.map{String(format: "%02X", $0)}.joined(separator: "")
//        print(peripheral.identifier.uuidString,"return",raw)
        data?.didReturnValueFrom(peripheral: peripheral, value: raw)
        
    }
    
    
    func didDiscoverPeripheralFinish() {
        connection?.didScanFinish(nearbyTags: TagConnection.shared.scannedTags)
        for tag in TagConnection.shared.tags where tag.peripheral != nil{
            ble.connectPeripheral(peripheral: tag.peripheral!)
        }
    }
    
    func didConnect(peripheral:CBPeripheral) {
        print("connected \(peripheral.identifier.uuidString)")
        connection?.didConnected(peripheral: peripheral)
        
    }
    
    func didFailToConnectPeripheral() {
        //
    }
    
    
    func didUpdateValueForCharacteristic(characteristic: CBCharacteristic, result: [UInt8]) {
        //
        
    }
    
    func didBLEStateChanged(central: CBCentralManager) {
        //
    }
    
    
}

