//
//  extension_API + JSON.swift
//  testAPI
//
//  Created by Chan Hin Hang on 15/12/2016.
//  Copyright © 2016 SGIVenture. All rights reserved.
//

import Foundation

extension Dictionary {
    
    var jsonString:String? {
        
        do {
            let stringData = try JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
            if let string = String(data: stringData, encoding: .utf8) {
                return string.replacingOccurrences(of: "\n", with: "")
            }
        }catch _ {
            
        }
        return nil
    }
}

//extension Sequence where Iterator.Element == [String: Any] {
//    
//    var jsonString:String? {
//        
//        do {
//            let stringData = try JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
//            if let string = String(data: stringData, encoding: .utf8) {
//                return string.replacingOccurrences(of: "\n", with: "")
//            }
//        }catch _ {
//            
//        }
//        return nil
//    }
//}

