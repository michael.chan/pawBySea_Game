//
//  API.swift
//  testAPI
//
//  Created by Chan Hin Hang on 6/12/2016.
//  Copyright © 2016 SGIVenture. All rights reserved.
//

import Foundation
import Alamofire
import UIKit

class API {
//    #if TEST
       //static let baseURLString =  "http://testing.petble.com/api/"
    
       static let baseURLString = "https://www.espetsso.com/hk/"
//    #else
//        static let baseURLString = "https://api.petble.com/api/"
//    #endif
    static var urlEnd = ""
    static var method: HTTPMethod!
    static var baseNSMutableURLRequest: URLRequest? {
        print(API.baseURLString + API.urlEnd)
        let mayChineseUrl = API.baseURLString +  (API.urlEnd.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "")
        
        if let URL = URL(string: mayChineseUrl){
            var urlRequest = URLRequest(url: URL as URL)
            urlRequest.httpMethod = API.method.rawValue
//            AppLog.log(tags:"API","Request",content:"URL: \(URL)")
            return urlRequest
        }
        
        return nil
    }
    
    static func baseParameters(mode: String) -> [String: Any] {
        return [:]
    }
    
    static func sendRequest(urlEnd:String,method:HTTPMethod, mode: String, parameters: [String: Any] = [:]) throws -> URLRequest {
//        var basicParameters = baseParameters(mode: mode)
//        basicParameters.merge(with: parameters ?? [:]) // merge two parameter

        
//        AppLog.log(tags:"API","Request",content:"Mode: \(mode) | Method: \(method.rawValue) | Parameters: \(basicParameters)")
        API.urlEnd = urlEnd
        API.method = method
        
        return try URLEncoding.default.encode(baseNSMutableURLRequest!, with: parameters.count > 0 ? parameters : nil)
    }
//    static func Post(mode: String, parameters: [String: Any]? = [String: Any]()) throws -> URLRequest {
//        var basicParameters = baseParameters(mode: mode)
//        basicParameters.merge(with: parameters!) // merge two parameter
//        print("POST: \(basicParameters)")
//        return try URLEncoding.default.encode(baseNSMutableURLRequest!, with: basicParameters.count > 0 ? basicParameters : nil)
//    }
}

enum APIError: Error {
    case noNetwork
    case network(error: Error)
    case server(error:Error)
    case dataSerialization(reason: String)
    case jsonSerialization(error: Error)
    case noContent(reason: String)
    case rscode(code: String ,reason: String)
    
}

extension DataRequest {
    public static func APIResponseSerializer(options: JSONSerialization.ReadingOptions = .allowFragments) -> DataResponseSerializer<Any> {
        return DataResponseSerializer { _, response, data, error in
            guard error == nil else {
                if response?.statusCode == 404 ||  (response?.statusCode)! / 100 == 5{
                    return .failure(APIError.server(error: error!))
                }else{
                    return .failure(APIError.network(error: error!))
                }
            }
            
            if let response = response , response.statusCode == 204 { return .success(NSNull()) }
            
            
            guard let validData = data , validData.count > 0 else {
                let failureReason = "JSON could not be serialized. Input data was nil or zero length."
                print(failureReason)
                return .failure(APIError.dataSerialization(reason: failureReason))
            }
            
            do {
                
                let JSON = try JSONSerialization.jsonObject(with: validData, options: options) as! [String: Any]
                
                if "\((JSON as AnyObject).value(forKey:"rscode")!)" != "0" {
                    if (("\((JSON as AnyObject).value(forKey:"rscode")!)" != "2") && ("\((JSON as AnyObject).value(forKey:"rscode")!)" != "5")){
//                        AppLog.log(tags: "API","Response", content: "RSCode fail reason: \(JSON)")
                    }
                    
                    return .failure(APIError.rscode(code:
                        "\((JSON as AnyObject).value(forKey:"rscode") ?? "")",
                        reason: "\((JSON as AnyObject).value(forKey:"error") ?? "")"))
                }
                return .success(JSON)
            } catch {
                return .failure(APIError.jsonSerialization(error: error as NSError))
            }
        }
    }
    
    public func responseAPI(
        queue: DispatchQueue? = nil,
        options: JSONSerialization.ReadingOptions = .allowFragments,
        completionHandler: @escaping(DataResponse<Any>) -> Void)
        -> Self {
            return response(
                queue: queue,
                responseSerializer: DataRequest.APIResponseSerializer(options: options),
                completionHandler: completionHandler
            )
    }
}

