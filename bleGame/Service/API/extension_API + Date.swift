//
//  extension_API + Date.swift
//  testAPI
//
//  Created by Chan Hin Hang on 15/12/2016.
//  Copyright © 2016 SGIVenture. All rights reserved.
//

import Foundation

extension Date{
    func dateString(format:String)->String{
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
}
