//
//  APIUsage.swift
//  testAPI
//
//  Created by Chan Hin Hang on 14/12/2016.
//  Copyright © 2016 SGIVenture. All rights reserved.
//

import Foundation
import UIKit
import Alamofire


protocol APIUsageDelegate :class{
//    func onAPIValidResponse(mode:String,response:[String:Any])
//    func onAPINetworkFailure(mode:String,err:Error,description:String)
//    func onRscodeNotOk(mode:String, code:String, reason:String)
    func onAPIRequestFeedback(request:ApiMode,feedback:ApiFeedback)
}

enum ApiFeedback{
    case success(response:[String:Any])
    case failure(reason:APIError)
    case empty
}

enum ApiMode{
    //SYSTEM API
//    case checkAppsStart(user_id:String?,user_token:String?)
    case postGameResult(results:[String:Any])
    case getGameUser(id:Int)
    //
    case postDiscount(email:String,discount:Int,token:String)
}

class ApiUsageClass{
    static var shared = ApiUsageClass()
    weak var delegate : APIUsageDelegate?
    var currentMode : String = ""
    var parameter: [String:Any] = [:]
    var timeout = false
    var retryCount = 0
    
    func use(api:ApiMode){
        var image:(key:String,image:UIImage)?
        var apiUrlEnd = ""
        var mode = ""
        var requestMethod : HTTPMethod = .post
        parameter.removeAll()
        
        //Setting request data to be submit
        switch api {
        case .postGameResult(let result):
            apiUrlEnd = "game_result"
            mode = "post_game_result"
            requestMethod = .post
            parameter = result
            
        case let .postDiscount( email, discount, token):
            apiUrlEnd = "marathon"
            mode = "post_discount"
            requestMethod = .get
            parameter = ["email":email,"reduction_percent":discount,"token":token]
            
        case .getGameUser(let id):
            apiUrlEnd = "game_user/\(id)"
            mode = "get_game_user"
            requestMethod = .get
            
//        default:
//            break
        }

        
        self.delay(delay: 10, closure: {
            
            print("delay")
            self.delegate?.onAPIRequestFeedback(request: api, feedback: .failure(reason: APIError.noNetwork))
          
        })
        
        if let _image = image{
            
            let URL = try! URLRequest(url: API.baseURLString + apiUrlEnd, method: requestMethod)
//            AppLog.log(tags:"API","Request",content:"URL: \(URL)")

            Alamofire.upload(multipartFormData: { multipartFormData in
                for (key, value) in self.parameter {
                    if let data = (value as! String).data(using: .utf8){
                        multipartFormData.append(data, withName: key)
                    }
                }
                if let photoData = UIImageJPEGRepresentation(_image.image.resizeImage(newWidth: 320) ?? _image.image , 0.9) {
//                    AppLog.log(tags:"API","Request",content:"POST Image with key \(_image.key)")
                    multipartFormData.append(photoData, withName: _image.key, fileName: "image.jpg", mimeType: "image/jpg")
                }

            }, with: URL, encodingCompletion: {
                encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        print("?",response)
                         self.responseHandler(mode: api, response: response)
                    }
                case .failure(_):
//                    AppLog.log(tags: "API","Response", content: "\(mode) : Encoding error\n")
                    self.delegate?.onAPIRequestFeedback(request: api, feedback: .failure(reason: APIError.noNetwork))
                }
            })
        
        }
        else{
            print("parameters:",parameter)
            let _ = Alamofire.request(try! API.sendRequest(urlEnd: apiUrlEnd, method:requestMethod, mode: mode ,parameters: parameter))
                .validate()
                .responseAPI { (response) in
                    print("<",response)
                    self.responseHandler(mode: api, response: response)
            }
        }
    }
    func delay(delay:Double, closure:@escaping ()->()) {
        if self.timeout{
            if #available(iOS 10.0, *) {
                let _ = Timer.scheduledTimer(withTimeInterval: 10.0, repeats: false) { (timer) in
                    closure()
                    timer.invalidate()
                }
            } else {
                DispatchQueue.main.asyncAfter(deadline: .now() + delay, execute: {
                    closure()
                })
            }
        }
    }
    
    //MARK:- RESPONSE HANDLER
    func responseHandler(mode: ApiMode,response:DataResponse<Any>){
        
        switch response.result{
            case .success(let value):
//                AppLog.log(tags: "API","Response", content: "\(mode): Valid JSON Response")
//                AppLog.log(tags: "API","Response", content: "JSON:\(value as! [String:Any])\n")
                self.delegate?.onAPIRequestFeedback(request: mode, feedback: .success(response: value as! [String:Any]))
                retryCount = 0
            case .failure(let error):
                switch error{
                case APIError.dataSerialization(_):
//                    AppLog.log(tags: "API","Response", content: "\(mode) : Data Serialization error\n")
                    self.delegate?.onAPIRequestFeedback(request: mode, feedback: .failure(reason: APIError.dataSerialization(reason: "")))
                    
                case APIError.server(let reason):
//                    AppLog.log(tags: "API","Response", content: "\(mode) : \("alert_network_error".localized())")
//                    AppLog.log(tags: "API","Response", content: "Reason : \(reason.localizedDescription)\n")
                    self.delegate?.onAPIRequestFeedback(request: mode, feedback: .failure(reason: .noNetwork))
                    
                case APIError.network(let reason):
//                    AppLog.log(tags: "API","Response", content: "\(mode) : \("alert_network_error".localized())")
//                    AppLog.log(tags: "API","Response", content: "Reason : \(reason.localizedDescription)\n")
                    self.delegate?.onAPIRequestFeedback(request: mode, feedback: .failure(reason: .noNetwork))
                    
                case APIError.jsonSerialization(_):
//                    AppLog.log(tags: "API","Response", content: "\(mode) : JSON Serialization error\n")
                    self.delegate?.onAPIRequestFeedback(request: mode, feedback: .failure(reason: .noNetwork))
                    
                case APIError.rscode(let code, let reason):
                    if (code == "2"){
//                        AppLog.log(tags: "API","Response", content: "\(mode) : Rscode2: Empty JSON\n")
                        self.delegate?.onAPIRequestFeedback(request: mode, feedback: .empty)
                    }
                    else if (code == "5"){
//                        AppLog.log(tags: "API","Response", content: "\(mode) : Rscode5 Reason: No active session\n")
                        
                    }
                    else{
                        self.delegate?.onAPIRequestFeedback(request: mode, feedback: .failure(reason: .rscode(code: code, reason: reason)))
                    }
                case APIError.noContent( _):
//                    AppLog.log(tags: "API","Response", content: "\(mode) : No content error\n")
                    self.delegate?.onAPIRequestFeedback(request: mode, feedback: .failure(reason: .noNetwork))
                
                default:break
                }
        }
    }
}
