//
//  Player.swift
//  bleGame
//
//  Created by SGI Mac Pro on 8/3/2018.
//  Copyright © 2018 SGI Mac Pro. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

public var mChess: [UIImage] = [UIImage(named:"blue2")!,UIImage(named:"red2")!,UIImage(named:"green2")!]

class Player{
    var id: Int
    var name: String
    var photo: String
    var player: UIImage?
    var position: CGPoint!
    var time = 0.0
    var assignedTo = 4
    var score = 0{
        didSet{
            self.process = Float(score) / Float(GameplayPara.destinationScore)
        }
    }
    var process:Float = 0.0
    var reachedPoint = 1
    var isActive = false
    
    init(id: Int,name:String,photo:String,player:Int,icon:UIImage){
        self.id = id
        self.name = name
        self.photo = photo
        self.assignedTo = player
        let bottomImage = icon.circularImage(size: CGSize(width: 120, height: 120))
    
        let topImage = mChess[player]
        
        let size = CGSize(width: 120, height: 176)
        UIGraphicsBeginImageContext(size)
        
        let areaSize = CGRect(x: 0, y: 0, width: 120, height: 120)
        bottomImage.draw(in: areaSize)
         let areaSize2 = CGRect(x: 0, y: 0, width: 120, height: 176)
        topImage.draw(in: areaSize2, blendMode: .normal, alpha: 1)
        
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        self.player = newImage
    }
    
    func passedPoint(){
        self.reachedPoint += 1
    }
    
    func updateImage(icon: UIImage){
        let topImage = mChess[self.assignedTo]
        if let url = URL(string:self.photo){
            KingfisherManager.shared.retrieveImage(with: url, options: nil, progressBlock: nil, completionHandler: {
                image,_,_,_ in
                    if let image = image{
                        let size = CGSize(width: 120, height: 176)
                        UIGraphicsBeginImageContext(size)
                        
                        let areaSize = CGRect(x: 0, y: 0, width: 120, height: 120)
                        image.circularImage(size: CGSize(width: 120, height: 120)).draw(in: areaSize)
                        
                        let areaSize2 = CGRect(x: 0, y: 0, width: 120, height: 176)
                        topImage.draw(in: areaSize2, blendMode: .normal, alpha: 1)
                        
                        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
                        UIGraphicsEndImageContext()
                        self.player = newImage
                }
            })
        }
        
        
    }
    
}
