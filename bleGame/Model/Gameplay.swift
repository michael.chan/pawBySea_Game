//
//  Gameplay.swift
//  bleGame
//
//  Created by SGI Mac Pro on 13/3/2018.
//  Copyright © 2018 SGI Mac Pro. All rights reserved.
//

import Foundation
import UIKit

class GamePlay{
    var startPoint: CGPoint!
    var playerPositions: [CGPoint] = []
    var players: [Player] = []
    
    
    init(startPoint _startPoint: CGPoint){
         startPoint = _startPoint
         playerPositions = [startPoint,startPoint,startPoint]
         players = []
    }
    
    
}
