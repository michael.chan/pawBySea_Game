//
//  SharedInstance.swift
//  bleGame
//
//  Created by SGI Mac Pro on 4/3/2018.
//  Copyright © 2018 SGI Mac Pro. All rights reserved.
//
import UIKit
import Foundation

enum GameplayInstance {
    static var tagsToBeConnected: [Tags] = []
    static var players: [Player] = [Player.init(id: 0, name: "", photo: "", player: 0, icon: UIImage()) ,Player.init(id: 0, name: "", photo: "", player: 1, icon: UIImage()),Player.init(id: 0, name: "", photo: "", player: 2, icon: UIImage())]
}

enum GameplayPara{
    static let destinationScore = 3000  // total score require to go to destination
    static let ptFor1chok = 40   // point got from 1 valid count
    static let chokValueThreshold = 300 // the change in value to be counted as valid
    static let timer = 10               // time in secord game lasts
}

enum GameplayAPI{
    static let postGameResult_url :String = "https://api.petble.com/api"
}
